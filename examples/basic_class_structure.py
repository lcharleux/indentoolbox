import numpy as np
import toml
import json
import yaml
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import leastsq

pd.options.display.float_format = "{:.5e}".format
import indentoolbox as itb
from indentoolbox.core import Batch, Test, Tip, UnloadingStep, ConicalLoadingStep


##
# DUMMY DATA CREATION
##

C = 2.0e9
m = 1.5
hmax = 100.0e-9
hf = 0.6 * hmax
delta = 12.0e-9
t_step = 1.0
N_frames = 1001
N_tests = 10
N_steps = 2
sigma_noise = {"force": 0.005 * C * hmax**2, "disp": 0.001 * hmax}

# DATA CREATION
time0 = np.linspace(0.0, t_step, N_frames)
disp0 = hmax / (time0.max() - time0.min()) * (time0 - time0.min())
force0 = C * (disp0 + delta) ** 2

time1 = time0 + time0.max()
disp1 = (hmax - hf) / (time1.max() - time1.min()) * (time1 - time1.min()) + hf
force1 = force0.max() * ((disp1 - disp1.min()) / (disp1.max() - disp1.min())) ** m

tests = []
for Nt in range(N_tests):
    steps = []

    data0 = {}
    data0["time"] = time0
    data0["disp"] = disp0 + np.random.normal(scale=sigma_noise["disp"], size=time0.size)
    data0["force"] = force0 + np.random.normal(
        scale=sigma_noise["force"], size=time0.size
    )
    steps.append(ConicalLoadingStep(data=pd.DataFrame(data0)))

    data1 = {}
    data1["time"] = time1
    data1["disp"] = (
        disp1 + np.random.normal(scale=sigma_noise["disp"], size=time1.size)
    )[::-1]
    data1["force"] = (
        force1 + np.random.normal(scale=sigma_noise["force"], size=time1.size)
    )[::-1]
    steps.append(UnloadingStep(data=pd.DataFrame(data1)))

    test = Test(steps=steps, metadata={"name": f"Test_{Nt}"})
    tests.append(test)
    plt.close("all")


batch_metadata = {"user": "Bob"}
batch = Batch(tests=tests, metadata=batch_metadata, tip=Tip())

plot_reject = False
plt.figure()
tests = batch.tests
Ntests = len(tests)
for Nt in range(Ntests):
    test = tests[Nt]
    if plot_reject == False and test.reject == False:
        test_data = test.data
        plt.plot(test_data.disp * 1.0e9, test_data.force * 1.0e6, "-", lw=1.0)

plt.grid()
plt.xlabel("Displacement, $h$ [nm]")
plt.ylabel("Force, $P$ [µN]")
plt.savefig("batch.png")


print("# LOADING " + 10 * "#")
loading_data = batch.collect_steps(0).parabolic_fit()
print(loading_data)
print("# UNLOADING " + 10 * "#")
unloading_data = batch.collect_steps(1).unloading_fit()
print(unloading_data)
