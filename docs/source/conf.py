# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "IndenToolBox"
copyright = "2023, L. Charleux, E. Roux, C. Elmo"
author = "L. Charleux, E. Roux, C. Elmo"
release = "0.1"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    # "myst_parser",
    "myst_nb",
    "sphinx.ext.autodoc",
    "nbsphinx",
    "sphinx.ext.mathjax",
    "sphinx_copybutton",
    "sphinxcontrib.bibtex",
    "matplotlib.sphinxext.plot_directive",
    "sphinx.ext.todo",
]

templates_path = ["_templates"]
exclude_patterns = []
# source_suffix = [".rst", ".md"]
source_suffix = {
    ".rst": "restructuredtext",
    ".ipynb": "myst-nb",
    ".myst": "myst-nb",
    ".md": "myst-nb",
}
master_doc = "index"
exclude_patterns = ["_build", "**.ipynb_checkpoints"]

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# html_theme = "alabaster"
# html_theme = "furo"
html_static_path = ["_static"]
html_sourcelink_suffix = ""
# html_theme = "nature"
html_theme = "sphinx_rtd_theme"
html_theme_options = {
    #'body_min_width': 0,
    #'body_max_width': None,
    "navigation_with_keys": True,
}

bibtex_bibfiles = ["biblio.bib"]
