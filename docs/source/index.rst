.. IndenToolBox documentation master file, created by
   sphinx-quickstart on Fri Jun 23 14:31:07 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to IndenToolBox's documentation!
========================================

Indentoolbox is an open-source tool based on the Python ecosystem, designed to handle indentation tests.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   setup
   tutorials
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
