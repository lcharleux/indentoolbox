import numpy as np


def is_scalar(a):
    """
    Checks if an object is scalar.
    """
    try:
        len(a)
        scalar = False
        a = np.array(a)
    except:
        scalar = True
        a = np.array([a])
    return scalar, a
