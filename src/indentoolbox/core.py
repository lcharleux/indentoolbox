"""
# indentoolbox.core.py
"""

import numpy as np
import pandas as pd
from indentoolbox.processing import parabolic_fit, unloading_fit
from indentoolbox.misc import is_scalar
from math import sin, cos, tan, pi, radians, degrees, sqrt
from dataclasses import dataclass
import toml
import indentoolbox as itb


class Container:
    """
    A container meta class for load and dump purposes.
    """

    _auto_dump_attr = []

    @classmethod
    def from_dict(cls, dic, class_map={}):
        """
        Loads a class instance from a dict/list structure.

        .. note::

            Needs better description.

        :param dic: The dictionary representation of the class instance.
        :param class_map: Optional class map for deserialization. Defaults to an empty dictionary.
        :type dic: dict
        :type class_map: dict
        :return: The loaded class instance.
        :rtype: Container
        """
        merged_class_map = dict(_default_class_map, **class_map)
        kwargs = {}
        for key, value in dic.items():
            if value.__class__ in [dict, list]:
                if key in merged_class_map:
                    _top_class = merged_class_map[key]
                else:
                    print(f"Warning: {key} is not a known config key")
                    _top_class = Container

            if value.__class__ == dict:
                if "__class__" in value.keys():
                    bottom_class = value["__class__"]
                    value.pop("__class__")
                    if bottom_class in merged_class_map.keys():
                        outer_class = merged_class_map[bottom_class]
                    else:
                        outer_class = _top_class
                else:
                    outer_class = _top_class
                kwargs[key] = outer_class.from_dict(value, class_map=class_map)
            elif value.__class__ == list:
                outer_class = _top_class
                kwargs[key] = outer_class.from_list(value, class_map=class_map)
            else:
                kwargs[key] = value
        if "__class__" in kwargs.keys():
            kwargs.pop("__class__")

        return cls(**kwargs)

    @classmethod
    def from_list(cls, lst, class_map={}):
        """
        Loads a list of class instances from a list structure.

        :param lst: The list representation of class instances.
        :param class_map: Optional class map for deserialization. Defaults to an empty dictionary.
        :type lst: list
        :type class_map: dict
        :return: The loaded list of class instances.
        :rtype: list[Container]
        """
        merged_class_map = dict(_default_class_map, **class_map)
        out = []
        for value in lst:
            if value.__class__ == dict:
                if "__class__" in value.keys():
                    outer_class = merged_class_map[value["__class__"]]
                else:
                    outer_class = cls
                out.append(outer_class.from_dict(value))
            elif value.__class__ == list:
                out.append(cls.from_list(value))
            else:
                out.append(value)
        return out

    def __init__(self, **kwargs) -> None:
        """
        Initializes a new instance of the `Container` class.

        :param kwargs: The key-value pairs to set as attributes of the class instance.
        :type kwargs: dict
        """
        for k, v in kwargs.items():
            setattr(self, k, v)

    def to_dict(self, out=None):
        """
        Converts the class instance to a dictionary representation.

        :param out: Optional dictionary to store the converted representation. Defaults to `None`.
        :type out: dict or None
        :return: The dictionary representation of the class instance.
        :rtype: dict
        """
        if out == None:
            out = {}
        out["__class__"] = self.__class__.__name__.lower()
        for key in self._auto_dump_attr:
            out[key] = getattr(self, key)
        return out

    def to_toml(self, path=None):
        """
        Converts the class instance to a TOML string representation.

        :param path: Optional path to save the TOML string as a file. Defaults to `None`.
        :type path: str or None
        :return: The TOML string representation of the class instance.
        :rtype: str
        """
        dic = self.to_dict()
        out = toml.dumps(dic)
        if path != None:
            with open(path, "w") as toml_file:
                toml_file.write(out)
        return out

    def data_to_csv(self, path=None):
        """
        Converts the data attribute of the class instance to a CSV string representation or saves it as a CSV file.

        :param path: Optional path to save the CSV string or file. Defaults to `None`.
        :type path: str or None
        :return: The CSV string representation of the data attribute.
        :rtype: str
        """
        csv = self.data.to_csv(path_or_buf=path, index=False)
        return csv

    def dump(self, root_path):
        """
        Dumps the class instance to TOML and CSV files.

        :param root_path: The root path for the output files.
        :type root_path: str
        :return: None
        """
        self.to_toml(root_path + ".toml")
        self.data_to_csv(root_path + ".csv")

    @classmethod
    def from_toml(cls, path, **kwargs):
        """
        Loads a class instance from a TOML file.

        :param path: The path to the TOML file.
        :type path: str
        :param kwargs: Additional keyword arguments to pass to `from_dict` method.
        :return: The loaded class instance.
        :rtype: Container
        """
        with open(path) as toml_file:
            dic = toml.load(toml_file)
        return cls.from_dict(dic, **kwargs)


class Step(Container):
    """
    Step class.

    :ivar data: The data associated with the step.
    :ivar parent: The parent object of the step.

    :param data: The data associated with the step.
    :param parent: The parent object of the step. Defaults to `None`.

    :raises: None

    :return: None
    """

    def get_index(self):
        """
        Return the index of the step within its parent's steps.

        :raises: None

        :return: The index of the step.
        """
        return self.parent.steps.index(self)

    def kind(self):
        """
        Return the kind of the step.

        :raises: None

        :return: The kind of the step.
        """
        return self.__class__.__name__

    def __init__(self, data, parent=None):
        """
        Initialize a `Step` object.

        :param data: The data associated with the step.
        :param parent: The parent object of the step. Defaults to `None`.

        :raises: None

        :return: None
        """
        self.data = data
        self.parent = parent

    def __repr__(self):
        """
        Return a string representation of the step.

        :raises: None

        :return: A string representation of the step.
        """
        name = self.__class__.__name__
        return f"{name}({len(self.data)} rows)"

    def get_tip(self):
        """
        Return the tip associated with the step.

        :raises: None

        :return: The tip associated with the step.
        """
        batch = self.batch
        if batch == None:
            return batch
        else:
            return batch.tip

    tip = property(get_tip)

    def get_test(self):
        """
        Return the test associated with the step.

        :raises: None

        :return: The test associated with the step.
        """
        if self.parent == None:
            return None
        else:
            return self.parent

    test = property(get_test)

    def get_batch(self):
        """
        Return the batch associated with the step.

        :raises: None

        :return: The batch associated with the step.
        """
        if self.parent == None:
            return None
        elif self.parent.parent == None:
            return None
        else:
            return self.parent.parent

    batch = property(get_batch)

    def get_corrected_disp(self):
        """
        Returns the corrected displacement based on the compliance provided.

        :raises: None

        :return: The corrected displacement for the step.
        """
        data = self.data
        compliance = self.batch.device.compliance
        disp = data.disp.values
        force = data.force.values
        return disp - force * compliance

    def to_dict(self, out=None):
        """
        Convert the step object to a dictionary representation.

        :param out: Optional output dictionary. Defaults to `None`.

        :raises: None

        :return: The dictionary representation of the step.
        """
        if out == None:
            out = {}
        return super().to_dict(out)

    @classmethod
    def from_dict(cls, dic, class_map={}):
        """
        Construct a `Step` object from a dictionary representation.

        :param dic: The dictionary representation of the step.
        :param class_map: Optional class map for deserialization. Defaults to an empty dictionary.

        :type dic: dict
        :type class_map: dict

        :raises: None

        :return: A `Step` object constructed from the dictionary representation.
        :rtype: Step
        """
        out = {}
        out["data"] = pd.DataFrame(dic["data"])
        return cls(**out)

    def get_id(self):
        """
        Get the ID of the step within its parent's steps.

        :return: The ID of the step within its parent's steps, or `None` if the step has no parent.
        :rtype: int or None
        """
        parent = self.parent
        if parent is not None:
            return parent.steps.index(self)

    def get_name(self):
        """
        Get the name of the step.

        :return: The name of the step in the format "test_{test_id}_step_{step_id}", or `None` if the step has no parent.
        :rtype: str or None
        """
        step_id = self.get_id()
        test_id = self.parent.get_id()
        return f"test_{test_id}_step_{step_id}"


class Test(Container):
    """
    Contains data for an indentation experiment.

    :ivar _auto_dump_attr: List of attributes to be automatically dumped.
    :vartype _auto_dump_attr: list

    :param steps: Each element is an instance of the Step class, containing data for one
                  segment (e.g., loading, holding, unloading, ..) of an indentation experiment.
    :type steps: list
    :param name: Name of the test.
    :type name: str, optional
    :param path_to_data: Path to the test data.
    :type path_to_data: str, optional
    :param hoffset: TODO, not clear to ab. Probably will allow offsetting displacement or force data.
                    TODO, rename as offset?
    :type hoffset: float, optional
    :param parent: Parent Batch to which the test belongs.
    :type parent: indentoolbox.core.Batch, optional
    :param reject: Flag to tag bad indentation experiments to be excluded for successive data analysis.
    :type reject: bool, optional
    """

    _auto_dump_attr = ["hoffset", "reject", "name", "path_to_data"]

    def __init__(
        self, steps, name="", path_to_data="", hoffset=0.0, parent=None, reject=False
    ):
        """
        Initializes a Test object.

        :param steps: Each element is an instance of the Step class, containing data for one
                      segment (e.g., loading, holding, unloading, ..) of an indentation experiment.
        :type steps: list
        :param name: Name of the test.
        :type name: str, optional
        :param path_to_data: Path to the test data.
        :type path_to_data: str, optional
        :param hoffset: TODO, not clear to ab. Probably will allow offsetting displacement or force data.
                        TODO, rename as offset?
        :type hoffset: float, optional
        :param parent: Parent Batch to which the test belongs.
        :type parent: indentoolbox.core.Batch, optional
        :param reject: Flag to tag bad indentation experiments to be excluded for successive data analysis.
        :type reject: bool, optional
        """
        self.name = name
        self.path_to_data = path_to_data
        self.hoffset = hoffset
        self.parent = parent
        self.steps = []
        for step in steps:
            self.add_step(step)
        self.reject = reject

    def get_index(self):
        """
        Get the index of the test within its parent batch.

        :return: Index of the test within its parent batch.
        :rtype: int
        """

        return self.parent.tests.index(self)

    def add_step(self, step):
        """
        Add a step to the test.

        :param step: Step instance to be added.
        :type step: Step
        """
        step.parent = self
        self.steps.append(step)

    def __repr__(self):
        """
        Return a string representation of the Test object.

        :return: String representation of the Test object.
        :rtype: str
        """
        return f"Test({len(self.steps)} steps)"

    def get_data(self):
        """
        Get the data from all steps of the test.

        :return: Concatenated data from all steps.
        :rtype: pandas.DataFrame
        """
        data = []
        steps = self.steps
        Nsteps = len(steps)
        for Ns in range(Nsteps):
            step = steps[Ns]
            step_data = step.data.copy()
            step_data["step"] = Ns
            data.append(step_data)
        return pd.concat(data)

    data = property(get_data)

    def to_dict(self, out=None):
        """
        Convert the Test object to a dictionary.

        :param out: Output dictionary to store the converted data.
        :type out: dict, optional
        :return: Dictionary representation of the Test object.
        :rtype: dict
        """
        if out == None:
            out = {}
        out["steps"] = [step.to_dict() for step in self.steps]
        return super().to_dict(out)

    @classmethod
    def from_txt(
        cls, content, protocol, name="", file_format="hysitron nano", **kwargs
    ):
        """
        Create a Test object from a text content in a specific format.

        :param content: Text content of the test.
        :type content: str
        :param protocol: List of step types in the indentation experiment.
        :type protocol: list
        :param name: Name of the test.
        :type name: str, optional
        :param file_format: Format of the text file (default: "hysitron nano").
        :type file_format: str, optional
        :param kwargs: Additional keyword arguments for the Test constructor.
        :type kwargs: Any
        :return: Test object created from the text content.
        :rtype: Test
        :raises ValueError: If the file format is not supported.

        .. note::
            Currently, only the "hysitron nano" file format is supported.

        .. note::
            The `protocol` parameter is a list ['Step', 'Step', ...] with as many elements
            as steps in the indentation experiment.

        .. note::
            The text content should be in the format of the specified file format.

        .. note::
            The `Step` class must be defined and accessible in the `itb.core` module.

        .. warning::
            The function assumes that the text content follows the expected format.
            Make sure the content is properly formatted before using this method.
        """
        if file_format == "hysitron nano":
            return cls.from_hysitron_nano(content, protocol, name, **kwargs)

        raise ValueError

    @classmethod
    def from_hysitron_nano(cls, content, protocol, name="", **kwargs):
        """
        Create a Test object from Hysitron nano format text content.

        :param content: Text content in Hysitron nano format.
        :type content: str
        :param protocol: List of step types in the indentation experiment.
        :type protocol: list
        :param name: Name of the test.
        :type name: str, optional
        :param kwargs: Additional keyword arguments for the Test constructor.
        :type kwargs: Any
        :return: Test object created from the Hysitron nano text content.
        :rtype: Test

        Reads a test text output in the Hysitron nano format and creates a corresponding Test object.

        .. note::
            The text content should be in the Hysitron nano format.

        .. note::
            The `protocol` parameter is a list ['Step', 'Step', ...] with as many elements
            as steps in the indentation experiment.

        .. note::
            The `Step` class must be defined and accessible in the `itb.core` module.

        .. warning::
            The function assumes that the text content follows the expected format.
            Make sure the content is properly formatted before using this method.
        """

        lines = content.strip().split("\n")
        disp, force, time = [[]], [[]], [[]]
        # Remove 3 non blank header lines

        # following block is used to detect first data line
        header_lines = 3
        Lcount = 0
        for start_pos, line in enumerate(lines):
            line = line.strip()
            if len(line) != 0:
                if Lcount == header_lines:
                    break
                else:
                    Lcount += 1

        for pos, line in enumerate(lines[start_pos:]):
            # detect if there is a blank line separating steps and if yes starts a new list for new step
            if line.split() == []:
                disp.append([])
                force.append([])
                time.append([])
            # if no blank line fill fill step line by line
            else:
                words = line.split()
                disp[-1].append(float(words[0]) * 1.0e-9)
                force[-1].append(float(words[1]) * 1.0e-6)
                time[-1].append(float(words[2]))

        # if protocol is None infer it. Protocol is a list ['Step', 'Step', ...] with as many elements
        # as steps in indentation experiment
        N_steps = len(time)
        if protocol != None:
            if len(protocol) != N_steps:
                print(
                    f"<Warning: protocol length {len(protocol)} does not match step number {N_steps}>"
                )
        else:
            protocol = ["Step" for i in range(N_steps)]

        # steps is a list in which element is a instance of Step class
        # Step.data contain actual indentation data (time, displacement, force)
        steps = []
        for i in range(N_steps):
            step_kind = protocol[i]
            if step_kind != None:
                step_data = pd.DataFrame()
                step_data["time"] = time[i]
                step_data["disp"] = disp[i]
                step_data["force"] = force[i]
                step_class = getattr(itb.core, step_kind)
                step = step_class(data=step_data)
                steps.append(step)

        test = cls(steps=steps, name=name, **kwargs)
        return test

    def get_id(self):
        """
        Get the index of the Test object within its parent Batch.

        :return: Index of the Test object within its parent Batch.
        :rtype: int or None

        Returns the index of the Test object within its parent Batch. If the Test object does not have a parent, None is returned.
        """
        parent = self.parent
        if parent is not None:
            return parent.tests.index(self)


class Batch(Container):
    """
    Batch class.

    :ivar tip: The tip associated with the batch.
    :ivar tests: A list of tests in the batch.
    :ivar device: The device used for the tests. If not provided, a default `Device` object is created.
    :ivar operators: A list of operators associated with the batch. If not provided, a default `Operator` object is created.
    :ivar sample: The sample used for the tests. If not provided, a default `Sample` object is created.
    :ivar date: The date of the batch.

    :param tip: The tip associated with the batch.
    :param tests: A list of tests to add to the batch.
    :param device: The device used for the tests. Defaults to `None`.
    :param operators: A list of operators associated with the batch. Defaults to `None`.
    :param sample: The sample used for the tests. Defaults to `None`.
    :param date: The date of the batch. Defaults to an empty string.

    :raises: None

    :return: None
    """

    _auto_dump_attr = []

    def __init__(self, tip, tests, device=None, operators=None, sample=None, date=""):
        """
        Initialize a `Batch` object.

        :param tip: The tip associated with the batch.
        :param tests: A list of tests to add to the batch.
        :param device: The device used for the tests. Defaults to `None`.
        :param operators: A list of operators associated with the batch. Defaults to `None`.
        :param sample: The sample used for the tests. Defaults to `None`.
        :param date: The date of the batch. Defaults to an empty string.

        :raises: None

        :return: None
        """
        self.tip = tip
        self.tests = []
        for test in tests:
            self.add_test(test)
        if device == None:
            device = Device()
        self.device = device
        if operators == None:
            operators = [Operator()]
        self.operators = operators
        if sample == None:
            sample = Sample()
        self.sample = sample
        self.date = date

    def add_test(self, test):
        """
        Add a test to the batch.

        :param test: The test to add.

        :raises: None

        :return: None
        """
        test.parent = self
        self.tests.append(test)

    def __repr__(self):
        """
        Return a string representation of the batch.

        :raises: None

        :return: A string representation of the batch.
        """
        return f"Batch({len(self.tests)} tests)"

    # steps = property(lambda self: StepMatrix(self.tests))
    def collect_steps(self, index):
        """
        Collect steps from all tests at the given index and return a `StepVector` object.

        :param index: The index of the steps to collect.

        :raises: None

        :return: A `StepVector` object containing the collected steps.
        """
        steps = [t.steps[index] for t in self.tests]
        return StepVector(steps, index=index)

    def to_dict(self, out=None):
        """
        Convert the batch object to a dictionary representation.

        :param out: Optional output dictionary. Defaults to `None`.

        :raises: None

        :return: The dictionary representation of the batch.
        """
        if out == None:
            out = {}
        out["tip"] = self.tip.to_dict()
        out["tests"] = [test.to_dict() for test in self.tests]
        out["device"] = self.device.to_dict()
        out["operators"] = [operator.to_dict() for operator in self.operators]
        out["sample"] = self.sample.to_dict()
        out["date"] = self.date
        return super().to_dict(out)

    def get_data(self):
        """
        Return the concatenated data from all tests as a pandas DataFrame.

        :raises: None

        :return: A pandas DataFrame containing the concatenated data from all tests.
        """
        data = []
        tests = self.tests
        Ntests = len(tests)
        for Nt in range(Ntests):
            test = tests[Nt]
            test_data = test.data.copy()
            test_data["test"] = Nt
            data.append(test_data)
        return pd.concat(data)

    data = property(get_data)

    @classmethod
    def load(cls, root_path):
        """
        Load a `Batch` object from the specified root path.

        The method reads CSV and TOML files to reconstruct the `Batch` object.

        :param root_path: The root path of the files to load (without extension).

        :raises: None

        :return: A `Batch` object loaded from the files.
        """
        batch_data = pd.read_csv(root_path + ".csv")
        dic = toml.load(open(root_path + ".toml"))
        batch_data = pd.read_csv(root_path + ".csv")
        dic = toml.load(open(root_path + ".toml"))
        for test_id, test_data in batch_data.groupby("test"):
            test_data.pop("test")
            for step_id, step_data in test_data.groupby("step"):
                step_data.pop("step")
                dic["tests"][test_id]["steps"][step_id]["data"] = step_data
        return cls.from_dict(dic)

    def load_dcmii(self, excel_file):
        """
        Load XP DCMII head results stored in Excel file export

        TODO: add Tests list, so far only Batch is imported

        Parameters
        ----------
        excel_file : str
            name of excel file

        Returns
        -------
        None.

        """
        # Read all sheets in your File
        sheets = pd.read_excel("0perc.xls", sheet_name=None)

        # only sheets containing data, starting by Test, are kept
        sheet_names = sheets.keys()
        sheet_names = [page for page in sheet_names if page.startswith("Test")]
        sheet_names.sort()

        standard_columns = ["time", "disp", "force", "step", "test"]
        special_columns = sheets[sheet_names[0]].columns[
            4:
        ]  # exclude segment and standard columns

        columns = (
            standard_columns + list(special_columns) + ["test name"]
        )  # we keep test name to easy the life of xp users

        tests = []
        for test_number, sheet_name in enumerate(sheet_names):
            df_test = pd.DataFrame(columns=columns)

            # getting markers for segment change
            active_sheet = sheets[sheet_name]

            steps = []
            markers = active_sheet["Segment"].dropna()
            for step_n, (line, line_next) in enumerate(
                zip(markers[:-1].index, markers[1:].index)
            ):
                if active_sheet.iloc[line]["Segment"] != "END":
                    sub_df = active_sheet.iloc[line:line_next]

                    names_std = [
                        "Time On Sample",
                        "Displacement Into Surface",
                        "Load On Sample",
                    ]
                    df_to_apnd = sub_df[names_std + list(special_columns)]
                    df_to_apnd.insert(3, "step", step_n)
                    df_to_apnd.insert(4, "test", test_number)
                    df_to_apnd.insert(len(df_to_apnd.columns), "test name", sheet_name)
                    # columns are renamed according to indentoolbox standard
                    df_to_apnd = df_to_apnd.rename(
                        columns={
                            "Time On Sample": "time",
                            "Displacement Into Surface": "disp",
                            "Load On Sample": "force",
                        }
                    )
                    # SI units conversion
                    df_to_apnd["force"] = df_to_apnd["force"] * 1e-3
                    df_to_apnd["disp"] = df_to_apnd["disp"] * 1e-9
                    df_to_apnd["Hardness"] = df_to_apnd["Hardness"] * 1e9
                    df_to_apnd["Modulus"] = df_to_apnd["Modulus"] * 1e9

                    this_step = Step(df_to_apnd)
                    steps.append(this_step)
                else:
                    break

            this_test = Test(steps)
            tests.append(this_test)
        self.tests = tests


class LoadingStep(Step):
    """
    Class representing a loading step in an indentation experiment.

    The LoadingStep class inherits from the Step class and represents a loading step in an indentation experiment.

    """

    pass


class UnloadingStep(Step):
    """
    Class representing an unloading step in an indentation experiment.

    This class inherits from the base class `Step` and provides specific functionality for unloading steps.

    Methods
    -------
    `unloading_fit(*args, **kwargs)`
        Perform unloading fitting on the unloading step.

    """

    def unloading_fit(self, *args, **kwargs):
        """
        Perform unloading fitting on the unloading step.

        This method performs unloading fitting on the unloading step using the provided arguments.

        Parameters
        ----------
        args
            Positional arguments to be passed to the unloading fitting function.
        kwargs
            Keyword arguments to be passed to the unloading fitting function.

        Returns
        -------
        object
            The result of the unloading fitting.

        """
        return unloading_fit(step=self, *args, **kwargs)


class ConicalLoadingStep(LoadingStep):
    """
    Represents a conical loading step in an indentation experiment.
    This class inherits from the `LoadingStep` class.

    Methods
    -------
    `parabolic_fit(*args, **kwargs)`
        Perform a parabolic fit on the conical loading step.
    """

    def parabolic_fit(self, *args, **kwargs):
        """
        Perform a parabolic fit on the conical loading step.

        This method calls the `parabolic_fit` function, passing the current
        `ConicalLoadingStep` instance as the `step` parameter, along with any
        additional arguments and keyword arguments.

        :param args: Additional positional arguments to be passed to the `parabolic_fit` function.
        :param kwargs: Additional keyword arguments to be passed to the `parabolic_fit` function.
        :return: The return value of the `parabolic_fit` function.
        """
        return parabolic_fit(step=self, *args, **kwargs)


class StepVector:
    """
    Represents a vector of steps in an indentation experiment.

    Parameters
    ----------
    steps : list
        A list of Step objects.
    index : int or None, optional
        The index of the StepVector.

    Methods
    -------
    __repr__()
        Returns a string representation of the StepVector.
    __getattr__(attr)
        Retrieves an attribute from each Step in the StepVector and returns an AttrArray.

    Attributes
    ----------
    index : int or None
        The index of the StepVector.
    steps : list
        A list of Step objects.

    """

    def __init__(self, steps, index=None):
        """
        Initialize a StepVector object.

        Parameters
        ----------
        steps : list
            A list of Step objects.
        index : int or None, optional
            The index of the StepVector.

        """
        self.index = index
        self.steps = steps

    def __repr__(self):
        """
        Return a string representation of the StepVector.

        Returns
        -------
        str
            A string representation of the StepVector showing the number of steps.

        """
        name = self.__class__.__name__
        return f"{name}({len(self.steps)} steps)"

    def __getattr__(self, attr):
        """
        Retrieve an attribute from each Step in the StepVector.

        This method returns an AttrArray object containing the requested attribute
        from each Step in the StepVector.

        Parameters
        ----------
        attr : str
            The name of the attribute to retrieve.

        Returns
        -------
        AttrArray
            An AttrArray object containing the requested attribute from each Step.

        """
        return AttrArray([getattr(s, attr) for s in self.steps])


class AttrArray:
    """
    Represents an array of attributes.

    Parameters
    ----------
    attrs : list
        A list of attributes.

    Methods
    -------
    `__call__(*args, **kwargs)`
        Calls each attribute with the given arguments and returns a concatenated DataFrame.

    Attributes
    ----------
    attrs : list
        A list of attributes.

    """

    def __init__(self, attrs):
        """
        Initialize an AttrArray object.

        Parameters
        ----------
        attrs : list
            A list of attributes.

        """
        self.attrs = attrs

    def __call__(self, *args, **kwargs):
        """
        Call each attribute with the given arguments and return a concatenated DataFrame.

        This method calls each attribute in the AttrArray with the provided arguments and
        returns a concatenated DataFrame where each attribute's result is a column in the
        DataFrame. The resulting DataFrame has a MultiIndex with levels "test" and "step".

        Parameters
        ----------
        *args : tuple
            Positional arguments to pass to each attribute.
        **kwargs : dict
            Keyword arguments to pass to each attribute.

        Returns
        -------
        pd.DataFrame
            A concatenated DataFrame where each attribute's result is a column.

        """
        df = pd.concat(list(map(lambda m: m(*args, **kwargs), self.attrs)), axis=1).T
        df.index.names = ("test", "step")
        return df


class Tip(Container):
    """
    Indentation Tip meta class.

    Parameters
    ----------
    young_modulus : float, optional
        Young's modulus of the tip material. Default is 1141.0e9.
    poisson_coefficient : float, optional
        Poisson's coefficient of the tip material. Default is 0.14.

    Attributes
    ----------
    young_modulus : float
        Young's modulus of the tip material.
    poisson_coefficient : float
        Poisson's coefficient of the tip material.

    Methods
    -------
    contact_area(h)
        Calculate the contact area of the tip at a given indentation depth.
    indentation_modulus()
        Calculate the indentation modulus of the tip.

    """

    _auto_dump_attr = ["young_modulus", "poisson_coefficient"]

    def __init__(self, young_modulus=1141.0e9, poisson_coefficient=0.14):
        """
        Initialize a Tip object.

        Parameters
        ----------
        young_modulus : float, optional
            Young's modulus of the tip material. Default is 1141.0e9.
        poisson_coefficient : float, optional
            Poisson's coefficient of the tip material. Default is 0.14.

        """
        self.young_modulus = young_modulus
        self.poisson_coefficient = poisson_coefficient

    def __repr__(self):
        """
        Initialize a Tip object.

        Parameters
        ----------
        young_modulus : float, optional
            Young's modulus of the tip material. Default is 1141.0e9.
        poisson_coefficient : float, optional
            Poisson's coefficient of the tip material. Default is 0.14.

        """
        name = self.__class__.__name__
        return f"{name}"

    def contact_area(self, h):
        """
        Calculate the contact area of the tip at a given indentation depth.

        Parameters
        ----------
        h : float
            Indentation depth.

        Returns
        -------
        float
            Contact area of the tip.

        """
        return pi * self.contact_radius(h) ** 2

    def indentation_modulus(self):
        """
        Calculate the indentation modulus of the tip E_ind = E / (1 - nu^2).

        Returns
        -------
        float
            Indentation modulus of the tip.

        """
        E = self.young_modulus
        nu = self.poisson_coefficient
        return E / (1 - nu**2)


class SpheroConicalTip(Tip):
    """
    Generic spheroconical tip.

    Parameters
    ----------
    angle : float, optional
        Half-angle of the spheroconical tip in degrees. Default is 70.3.
    htrunc : float, optional
        Truncation height of the spheroconical tip. If not provided, the radius should be specified instead.
    radius : float, optional
        Radius of the spheroconical tip. If not provided, the truncation height should be specified instead.
    *args, **kwargs
        Additional arguments to be passed to the parent class.

    Attributes
    ----------
    angle : float
        Half-angle of the spheroconical tip in degrees.
    htrunc : float
        Truncation height of the spheroconical tip.
    radius : float
        Radius of the spheroconical tip.

    Methods
    -------
    get_htrunc()
        Get the truncation height of the spheroconical tip.
    set_htrunc(htrunc)
        Set the truncation height of the spheroconical tip.
    get_hcrit()
        Get the critical height of the spheroconical tip.
    contact_radius(h)
        Calculate the contact radius of the spheroconical tip at a given indentation depth.

    """

    def __init__(self, angle=70.3, htrunc=None, radius=None, *args, **kwargs):
        """
        Initialize a SpheroConicalTip object.

        Parameters
        ----------
        angle : float, optional
            Half-angle of the spheroconical tip in degrees. Default is 70.3.
        htrunc : float, optional
            Truncation height of the spheroconical tip. If not provided, the radius should be specified instead.
        radius : float, optional
            Radius of the spheroconical tip. If not provided, the truncation height should be specified instead.
        *args, **kwargs
            Additional arguments to be passed to the parent class.

        """
        super().__init__(*args, **kwargs)
        self.angle = angle
        if radius == None:
            if htrunc != None:
                self.htrunc = htrunc
            else:
                self.radius = 0.0
        else:
            self.radius = radius

        self._auto_dump_attr = super()._auto_dump_attr + ["angle", "htrunc", "radius"]

    def __repr__(self):
        """
        Return a string representation of the SpheroConicalTip object.

        Returns
        -------
        str
            A string representation of the SpheroConicalTip object.

        """
        out = super().__repr__()
        out += f"(radius={self.radius:.3e}, htrunc={self.htrunc:.2e}, hcrit={self.hcrit:.2e})"
        return out

    def get_htrunc(self):
        """
        Get the truncation height of the spheroconical tip.

        Returns
        -------
        float
            Truncation height of the spheroconical tip.

        """
        return self.radius * (1.0 / sin(radians(self.angle)) - 1.0)

    def set_htrunc(self, htrunc):
        """
        Set the truncation height of the spheroconical tip.

        Parameters
        ----------
        htrunc : float
            Truncation height of the spheroconical tip.

        """
        self.radius = htrunc / (1.0 / sin(radians(self.angle)) - 1.0)

    htrunc = property(get_htrunc, set_htrunc)

    def get_hcrit(self):
        """
        Get the critical height of the spheroconical tip.

        Returns
        -------
        float
            Critical height of the spheroconical tip.

        """
        return self.radius * (1.0 - sin(radians(self.angle)))

    hcrit = property(get_hcrit)

    def contact_radius(self, h):
        """
        Calculate the contact radius of the spheroconical tip at a given height.

        Parameters
        ----------
        h : float or array-like
            Height(s) at which to calculate the contact radius.

        Returns
        -------
        float or array-like
            Contact radius(es) of the spheroconical tip.

        """
        R, htrunc, hcrit = self.radius, self.htrunc, self.hcrit
        angle = self.angle
        scalar, h = is_scalar(h)
        rc = np.where(
            h < hcrit, np.sqrt(h * (2 * R - h)), tan(radians(angle)) * (h + htrunc)
        )
        if scalar:
            rc = rc[0]
        return rc


@dataclass
class Device(Container):
    """
    Indentation device class.
    """

    name: str = ""
    institute: str = ""
    compliance: str = ""
    provider: float = 0.0

    def __post_init__(self):
        self._auto_dump_attr = ["name", "institute", "compliance", "provider"]


@dataclass
class Operator(Container):
    """
    Operator class.
    """

    name: str = ""
    institute: str = ""

    def __post_init__(self):
        self._auto_dump_attr = ["name", "institute"]


@dataclass
class Sample(Container):
    """
    Operator class.
    """

    name: str = ""
    provider: str = ""

    def __post_init__(self):
        self._auto_dump_attr = ["name", "provider"]


class _DataFrame(Container):
    @classmethod
    def from_dict(cls, dic, class_map=None):
        return pd.DataFrame.from_dict(dic)


_default_class_map = {
    "batch": Batch,
    "tip": Tip,
    "test": Test,
    "tests": Test,
    "sample": Sample,
    "operator": Operator,
    "operators": Operator,
    "device": Device,
    "data": _DataFrame,
    "step": Step,
    "steps": Step,
    "conicalloadingstep": ConicalLoadingStep,
    "unloadingstep": UnloadingStep,
    "spheroconicaltip": SpheroConicalTip,
}
