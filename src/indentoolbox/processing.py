# INDENTOOLBOX / PROCESSING
import numpy as np
from scipy.optimize import leastsq
import pandas as pd
from scipy import integrate, optimize


def step_fit(step, out=None):
    if out == None:
        out = {}
    disp = step.get_corrected_disp()
    force = step.data.force.values
    if len(disp) > 1:
        W = integrate.simpson(force, disp)
    else:
        out["W"] = np.nan
    out["W"] = W
    return step_series(step, out)


def parabolic_fit(step, htrunc=None, displim=None, show_reject=True):
    disp = step.get_corrected_disp()
    force = step.data.force.values
    if displim != None:
        loc = disp >= displim
        disp = disp[loc]
        force = force[loc]
    max_disp = disp.max()
    max_force = force.max()
    forcer = np.sign(force) * np.sqrt(np.abs(force))
    if htrunc == None:
        out = np.polyfit(disp, forcer, 1)
        C = out[0] ** 2
        htrunc = out[1] / out[0]
    else:
        C = ((forcer / disp + htrunc) ** 2).mean()
    test = step.test
    if test == None:
        reject = 0
    else:
        reject = test.reject * 1
    step_index = step.get_index()
    test_index = step.parent.get_index()
    out = {"C": C, "htrunc": htrunc, "Pm": max_force, "hm": max_disp, "reject": reject}
    # return step_series(step, out)
    return step_fit(step, out)


def unloading_fit(step, force_threshold=None, maxfev=10000):
    disp = step.get_corrected_disp()
    force = step.data.force.values
    max_force = force.max()
    max_disp = disp.max()
    if force_threshold != None:
        loc = np.where(force >= max_force * force_threshold)
        disp = disp[loc]
        force = force[loc]

    def func(k, x):
        xu = x - k[0]
        xu = np.where(xu > 0.0, xu, 0.0)
        return max_force * (xu / (max_disp - k[0])) ** k[1]

    err = lambda v, x, y: (func(v, x) - y)
    k0 = [max_disp / 2.0, 1.5]
    k, success = leastsq(err, k0, args=(disp, force), maxfev=maxfev)
    final_disp = k[0]
    contact_stiffness = k[1] * max_force / (max_disp - k[0])
    unload_exponent = k[1]
    test = step.test
    if test == None:
        reject = 0.0
    else:
        reject = test.reject * 1
    out = {
        "hf": final_disp,
        "S": contact_stiffness,
        "m": unload_exponent,
        "Pm": max_force,
        "hm": max_disp,
        "success": success,
        "reject": reject,
    }
    # return step_series(step, out)
    return step_fit(step, out)


def step_series(step, dic):
    """
    Creates a pandas.Series object with the (test, step) naming.
    """
    step_index = step.get_index()
    test_index = step.parent.get_index()
    return pd.Series(dic, name=(test_index, step_index))


def OliverPharr(data, tip, disp_key="hm", force_key="Pm", epsilon=0.72, beta=1.04):
    """
    Perform Oliver-Pharr analysis on indentation data.

    Parameters
    ----------
    data : pandas.DataFrame
        DataFrame containing indentation data.
    tip : Tip
        Tip object representing the indentation tip.
    disp_key : str, optional
        Key for the displacement column in the data DataFrame. Default is "hm".
    force_key : str, optional
        Key for the force column in the data DataFrame. Default is "Pm".
    epsilon : float, optional
        Constant factor for calculating the contact depth. Default is 0.72.
    beta : float, optional
        Constant factor for calculating the reduced modulus. Default is 1.04.

    Returns
    -------
    pandas.DataFrame
        DataFrame containing Oliver-Pharr analysis results with the following columns:
        - 'h': Displacement values from the data DataFrame.
        - 'P': Force values from the data DataFrame.
        - 'S': Stiffness values from the data DataFrame.
        - 'hc': Contact depth calculated using the Oliver-Pharr method.
        - 'Ac': Contact area calculated using the indentation tip.
        - 'Eeq': Equivalent elastic modulus.
        - 'Eeqsamp': Sample elastic modulus corrected for the indentation tip.

    """
    out = pd.DataFrame()
    out["h"] = data[disp_key]
    out["P"] = data[force_key]
    out["S"] = data["S"]
    out["hc"] = out.h - epsilon * out["P"] / out["S"]
    out["Ac"] = tip.contact_area(out.hc.values)
    Eeq = out["S"] / (2.0 * beta * np.sqrt(out["Ac"] / np.pi))
    out["Eeq"] = Eeq
    E_indtip = tip.indentation_modulus()
    out["Eeqsamp"] = (Eeq**-1 - E_indtip**-1) ** -1
    return out


def not_null_or_nan(x):
    """
    Returns True if is not null and not nan
    """
    out = True
    if x == 0.0:
        out = False
    if np.isnan(x):
        out = False
    return out


def find_root(func, x0, x1, maxiter=2000, debug=False, npoints=10000):
    """
    Finds several roots between x0 and x1 or returns nan. Func(x0) and Func(x1) must have opposite signs.
    """
    #  if debug:
    #    plt.plot(func, x0, x1, npoints)

    x = np.linspace(x0, x1, npoints)
    y = func(x)
    ys = np.sign(y)
    yc = 0.5 * abs(ys[1:] - ys[:-1])
    loc = np.where(yc == 1)[0]
    out = []
    for l in loc:
        out.append(optimize.brentq(func, x[l], x[l + 1], maxiter=maxiter))

    if debug:
        plt.plot(x, y, label="func")
        plt.plot(np.asarray(out), func(np.asarray(out)), "or", label="root")
        plt.plot([x0, x1], [0, 0], "k")
        plt.grid()
        # plt.legend()

    return out


def GIAN99(hm, hf, S, C, nu=0.3, E_ind=np.inf, nu_ind=0.1, *args, **kwargs):
    """
    Perform elastoplastic analysis using the GIAN99 method.

    This method refers to the following article:

    * Title: Determination of elastoplastic properties by instrumented sharp indentation
    * Authors: Giannakopoulos A.E.; Suresh S.
    * Year: 1999
    * DOI: 10.1016/S1359-6462(99)00011-1
    * URL: http://www.sciencedirect.com/science/article/pii/S1359646299000111

    Parameters
    ----------
    hm : float
        Maximum penetration depth.
    hf : float
        Final unloading penetration depth.
    S : float
        Stiffness.
    C : float
        Contact compliance.
    nu : float, optional
        Poisson's ratio. Default is 0.3.
    E_ind : float, optional
        Indentation modulus. Default is np.inf.
    nu_ind : float, optional
        Indentation Poisson's ratio. Default is 0.1.
    *args
        Additional positional arguments.
    **kwargs
        Additional keyword arguments.

    Returns
    -------
    dict
        Dictionary containing the following outputs:
        - 'dsigma': Plastic displacement.
        - 'Eeq': Equivalent elastic modulus.
        - 'Ac': Contact area.
        - 'k0': Reduced elastic modulus.
        - 'sy': Yield strength.
        - 'n': Work-hardening exponent.

    """
    # Constants
    d = 5.0
    M1 = 7.143
    M2 = -1.0
    # Reverse algorithm
    # Step 3, eq5 in GIAN99
    k0 = (
        1.0 / d * (1.0 - hf / hm)
    )  # k0 is noted "S" in the original article but this notation is conflicting to it was changed to "k0"

    # Step 4, eq2 in GIAN99
    Ac = hm**2 * (
        009.96
        - 012.64 * (1.0 - k0)
        + 105.42 * (1.0 - k0) ** 2
        - 229.57 * (1.0 - k0) ** 3
        + 157.67 * (1.0 - k0) ** 4
    )

    # Step 5,
    if "Eeq" in kwargs.keys():
        Eeq = kwargs["Eeq"]
    else:
        # eq 3 in GIAN99, where S is the slope (S=dP/dh)
        Eeq = 1.0 / 1.142 * S / Ac**0.5  # here Eeq is supposed to be unknown

    pav = k0 * Eeq
    # Step 6, eq4 in GIAN99
    dsigma = 0.29 * Eeq * (1.0 - 0.142 * (hf / hm) - 0.957 * (hf / hm) ** 2)
    # Step 7,eq 1 in GIAN99
    f = (
        lambda sy: M1
        * ((sy + dsigma) * (1.0 + sy / (sy + dsigma)) * (M2 + np.log(Eeq / sy)))
        - C
    )
    sy = find_root(f, pav / 3.0, 2.0 * pav)
    s029 = [s + dsigma for s in sy]
    # Find n
    Eeq_ind = E_ind / (1 - nu_ind**2)
    E = (1 - nu**2) * (Eeq**-1 - Eeq_ind**-1) ** -1
    n = []
    for i in range(len(sy)):
        n.append(np.log(s029[i] / sy[i]) / np.log((0.29 * E + s029[i]) / sy[i]))
    # Output dict
    out = {}
    out["dsigma"] = [dsigma for i in range(len(sy))]
    out["Eeq"] = [Eeq for i in range(len(sy))]
    out["Ac"] = [Ac for i in range(len(sy))]
    out["k0"] = [k0 for i in range(len(sy))]
    out["sy"] = sy
    out["n"] = n
    return out


def DAO01(S, C, Pm=0.0, Wfrac=0.0, hm=0.0, hf=0.0, *args, **kwargs):
    """
    Perform forward and reverse analysis using the DAO01 method.

    This method refers to the following article:

    * Title: Computational modeling of the forward and reverse problems in instrumented sharp indentation
    * Authors: Dao, M.; Chollacoop, N.; Van Vliet, K. J.; Venkatesh, T. A.; Suresh, S.
    * Year: 2001
    * DOI: 10.1016/S1359-6454(01)00295-6
    * URL: http://www.sciencedirect.com/science/article/pii/S1359645401002956

    Parameters
    ----------
    S : float
        Unloading stiffness (needed data).
    C : float
        Curvature of the loading curve (needed data).
    Pm : float, optional
        Maximum load.
        Default is 0.0.
    Wfrac : float, optional
        Ratio of the plastic work to the total work.
        Default is 0.0.
    hm : float, optional
        Maximum penetration depth.
        Default is 0.0.
    hf : float, optional
        Final unloading penetration depth.
        Default is 0.0.
    *args
        Additional positional arguments.
    **kwargs
        Additional keyword arguments.

    Returns
    -------
    dict
        Dictionary containing the following outputs:

        - 'sy' : list
            Yield strength.
        - 'n' : list
            Work-hardening exponent.
        - 'Eeq' : float
            Equivalent elastic modulus.
        - 'E' : float
            Elastic modulus.
        - 'sigma0033' : float
            True stress at 0.033% offset.
        - 'Pav' : float
            Average pressure.

    """
    # Constants
    c = 1.2370  # tab2, Berkovich large def

    # Check inputs :
    if (hm == 0.0) & (Pm == 0.0):
        print("hm or Pm are needed")
    if (hf == 0.0) & (Wfrac == 0.0):
        print("hf or Wfrac are needed")

    if Pm == 0.0:
        Pm = C * hm**2.0
    else:
        hm = (Pm / C) ** 0.5

    # PI functions
    # eq B1
    pi1 = lambda Eeq, sigma0033: (
        -1.131 * np.log(Eeq / sigma0033) ** 3
        + 13.635 * np.log(Eeq / sigma0033) ** 2
        - 30.594 * np.log(Eeq / sigma0033)
        + 29.267
    )

    # eq B4, hfrac = hf / hm
    pi4 = lambda hfrac: 0.268536 * (0.9952495 - hfrac) ** 1.1142735

    # eq B5, hfrac = hf / hm
    pi5 = lambda hfrac: 1.61217 * (
        1.13111
        - 1.74756 ** (-1.49291 * hfrac**2.535334)
        - 0.075187 * hfrac**1.135826
    )

    # if Wp/Wt is provide or h
    if Wfrac != 0.0:
        f5 = lambda hfrac: pi5(hfrac) - Wfrac
        hfrac = find_root(f5, 0.0, 1.0, debug=False)
        print(hfrac)
        hfrac = hfrac[0]
    else:
        hfrac = hf / hm

    # Reverse algorithm
    # Solve for Ac and Eeq
    Ac = (Pm * c / S / pi4(hfrac)) ** 2
    Eeq = S / c / Ac**0.5
    Pav = Pm / Ac

    nu = 0.3
    E = Eeq * (1.0 - nu**2)

    # Solve for sigma0033
    f4 = lambda sigma0033: sigma0033 * pi1(Eeq, sigma0033) - C
    sigma0033 = find_root(f4, 1.0e-5 * Eeq, Eeq / 10, debug=False)

    if len(sigma0033) > 1:
        print("multiple roots to solve sigma0033")
        sigma0033 = sigma0033[0]
    elif len(sigma0033) < 1:
        print("no root to solve sigma0033")
        sigma0033 = np.nan
    else:
        sigma0033 = sigma0033[0]

    # Solve for n
    # Pi2 function is defined here because it depends on Eeq ans sigma0033.
    if not np.isnan(sigma0033):
        pi2_poly = (
            np.poly1d([-1.40557, 0.77526, 0.15830, -0.06831])
            * np.log(Eeq / sigma0033) ** 3.0
            + np.poly1d([17.93006, -9.22091, -2.37733, 0.86295])
            * np.log(Eeq / sigma0033) ** 2.0
            + np.poly1d([-79.99751, 40.55620, 9.00157, -2.54543])
            * np.log(Eeq / sigma0033)
            + np.poly1d([122.65069, -63.88418, -9.58936, 6.20045])
            - S / Eeq / hm
        )
        # Determination of n:
        n = find_root(pi2_poly, 0, 1, debug=False)
        # n = pi2_poly.r
        # n = n[np.where(np.isreal(n))].real

        sy = []
        if len(n) < 1:
            print("no root to solve n")
            sy.append(sigma0033)
            n.append(0.0)
        else:
            for i in range(len(n)):
                f = (
                    lambda sigmay: sigmay * (1.0 + E / sigmay * 0.033) ** n[i]
                    - sigma0033
                )
                sy_i = find_root(f, 1e-6 * sigma0033, sigma0033, debug=False)
                if len(sy_i) == 0:  # no root in f
                    sy.append(0.0)
                else:
                    sy.append(sy_i[0])
    else:
        n = [np.nan]
        sy = [np.nan]

    # Output dict
    out = {}
    out["sy"] = sy
    out["n"] = n
    out["Eeq"] = Eeq
    out["E"] = E
    out["sigma0033"] = sigma0033
    out["Pav"] = Pav
    return out


def CASA05(hm, hf, Pm, S, C, nu=0.3, Ac=None):
    """
    Perform mechanical property extractions using the CASA05 method.

    This method refers to the following article:

    * Title: The duality in mechanical property extractions from Vickers and Berkovich instrumented indentation experiments
    * Authors: Casals, O.; Alcala, J.
    * Year: 2005
    * DOI: 10.1016/j.actamat.2005.03.051
    * URL: http://www.sciencedirect.com/science/article/pii/S1359645405002181

    Parameters
    ----------
    hm : float
        Maximum penetration depth.
    hf : float
        Final unloading penetration depth.
    Pm : float
        Maximum load.
    S : float
        Unloading stiffness (needed data).
    C : float
        Curvature of the loading curve (needed data).
    nu : float, optional
        Poisson's ratio.
        Default is 0.3.
    Ac : float or None, optional
        Contact area.
        Default is None.

    Returns
    -------
    dict
        Dictionary containing the following outputs:

        - 'sigma_005' : float or nan
            True stress at 0.005% offset.
        - 'sigma_008' : float or nan
            True stress at 0.008% offset.
        - 'sy' : float or nan
            Yield strength.
        - 'n' : float or nan
            Work-hardening exponent.
        - 'E_eq' : float or nan
            Equivalent elastic modulus.
        - 'E' : float or nan
            Elastic modulus.

    """

    out = {}
    # In case of failure, nans are returned
    sigma_005, sigma_008 = np.nan, np.nan
    sigma_y = np.nan
    E_eq, E = np.nan, np.nan
    n = np.nan
    # EXTRA DATA
    he = hm - Pm / S
    # ASSUMPTIONS
    # nu = 0.3  # Assumed by the authors
    # CONSTANTS
    f = 24.5  # Tip are function, Vickers or mod. Berk here.
    eta = 0.8261
    # PI functions
    A_Ksi1 = np.array([-1.6857, 1.9367, -0.8110, 0.3667])
    B_Ksi1 = np.array([4.1116, -4.7514, 1.9980, -0.5751])
    C_Ksi1 = np.array([-2.4293, 2.8186, -1.1875, 0.2078])
    Ksi1 = np.poly1d(A_Ksi1 + B_Ksi1 * (hf / hm) + C_Ksi1 * (hf / hm) ** 2)

    A_Ksi2 = np.array([23.911, -15.021, -3.5430, 2.9100])
    B_Ksi2 = np.array([-60.311, 34.342, 13.372, -7.4710])
    C_Ksi2 = np.array([35.792, -17.368, -12.239, 6.2350])
    Ksi2 = np.poly1d(A_Ksi2 + B_Ksi2 * (hf / hm) + C_Ksi2 * (hf / hm) ** 2)

    Pi8 = Ksi1**2 * Ksi2

    A_Pi8_bar = 0.9358
    B_Pi8_bar = -1.6781
    C_Pi8_bar = 0.9931
    Pi8_bar = A_Pi8_bar + B_Pi8_bar * (hf / hm) + C_Pi8_bar * (hf / hm) ** 2

    A_Phi11 = np.poly1d([2.5728, -3.8670, 3.3795, 2.4959])
    B_Phi11 = np.poly1d([0.0, 0.0, -1.0209, -0.0678])
    C_Phi11 = np.poly1d([0.0, -371.51, 54.847, 154.53])
    D_Phi11 = np.poly1d([0.0, -9.3054, 2.0426, -1.5249])
    E_Phi11 = np.poly1d([405.79, -405.29, 74.601, 22.900])
    Phi11 = lambda E_eq, sigma_005, n: A_Phi11(n) + B_Phi11(n) * np.exp(
        -E_eq / sigma_005 / C_Phi11(n)
    ) * D_Phi11(n) * np.exp(-E_eq / sigma_005 / E_Phi11(n))

    A_Pi11 = np.poly1d([191.51, -170.86, -48.700, 99.950])
    B_Pi11 = np.poly1d([0.0, 13.790, -6.7200, -17.530])
    C_Pi11 = np.poly1d([4130.8, -3053.7, -634.82, 618.58])
    D_Pi11 = np.poly1d([-1101.5, 769.58, -75.347, -52.630])
    E_Pi11 = np.poly1d([215.78, -152.14, -119.31, 92.990])
    Pi11 = lambda E_eq, sigma_008, n: A_Pi11(n) + B_Pi11(n) * np.exp(
        -E_eq / sigma_008 / C_Pi11(n)
    ) * D_Pi11(n) * np.exp(-E_eq / sigma_008 / E_Pi11(n))

    # Reverse algorithm
    if Ac == None:
        # STEP 1: FIND n
        f0 = Pi8 - 1.0 / (eta**2 * f) * (1.0 - he / hm) ** 2
        raw_n = f0.r
        raw_n = raw_n[np.where(np.isreal(raw_n))].real
        raw_n = raw_n[np.where(raw_n <= 0.6)]
        raw_n = np.where(raw_n < 0.0, 0.0, raw_n)
        raw_n = np.unique(raw_n)
        raw_n.sort()
        if len(raw_n) == 0:
            n = np.array([Pi8_bar, Pi8_bar])
        if len(raw_n) == 1:
            n = np.array([raw_n[0], raw_n[0]])
        if len(raw_n) == 2:
            n = raw_n
        if len(raw_n) >= 3:
            n = raw_n[:2]
        # STEP 2: FIND p_bar and E_eq
        p_bar = (
            np.array([Ksi1(ni) ** 2 for ni in n]) * eta**2 * C * (1 - he / hm) ** -2
        )
        E_eq = p_bar**0.5 * eta * C**0.5 * (1 - he / hm) ** -1
        # STEP 3: FIND alpha
        alpha = C / f / p_bar
        # STEP 4: FIND sigma_005 AND sigma_008
        sigma_005, sigma_008 = np.array([np.nan, np.nan]), np.array([np.nan, np.nan])
        for j in range(2):
            f0 = (
                lambda sigma_005: Phi11(E_eq[j], sigma_005, n[j]) - p_bar[j] / sigma_005
            )
            sigma_005[j] = find_root(f0, E_eq[j] * 1.0e-5, E_eq[j])[0]
            f1 = lambda sigma_008: Pi11(E_eq[j], sigma_008, n[j]) - C / sigma_008
            sigma_008[j] = find_root(f1, E_eq[j] * 1.0e-5, E_eq[j])[0]

        # STEP 5: FIND E
        E = E_eq * (1.0 - nu**2)
        # STEP 6: FIND sigma_y
        sigma_y = np.array([np.nan, np.nan])
        for j in range(2):
            if sigma_008[j] != np.nan:
                f2 = (
                    lambda sigma_y: sigma_y * (1.0 + E[j] / sigma_y * 0.08) ** n[j]
                    - sigma_008[j]
                )
                roots = find_root(f2, E[j] * 1.0e-5, sigma_008[j])
                if len(roots) > 0:
                    sigma_y[j] = roots[0]

    else:  # Ac != None
        if not_null_or_nan(Ac):
            p_bar = Pm / Ac
            E_eq = p_bar**0.5 * eta * C**0.5 * (1.0 - he / hm) ** -1
            if not_null_or_nan(E_eq):
                E = E_eq * (1.0 - nu**2)
                raw_n = (Ksi1 - p_bar / E_eq).r
                raw_n = raw_n[np.where(raw_n <= 0.6)]
                raw_n = raw_n[np.where(np.isreal(raw_n))].real
                raw_n = np.where(raw_n < 0.0, 0.0, raw_n)
                raw_n = np.unique(raw_n)
                raw_n.sort()
                if len(raw_n) > 0:
                    n = raw_n[0]

                    def f1(sigma_008):
                        return Pi11(E_eq, sigma_008, n) - C / sigma_008

                    sigma_008 = find_root(f1, E * 1.0e-5, E_eq)[0]
                    if sigma_008 != np.nan:

                        def f2(sigma_y):
                            return sigma_y * (1.0 + E / sigma_y * 0.08) ** n - sigma_008

                        sigma_y = find_root(f2, E * 1.0e-5, sigma_008)[0]

    out["sigma_005"] = sigma_005
    out["sigma_008"] = sigma_008
    out["sy"] = sigma_y
    out["n"] = n
    out["E_eq"] = E_eq
    out["E"] = E
    return out
